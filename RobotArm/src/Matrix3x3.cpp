#include "Matrix3x3.h"

#pragma region Constructors
Matrix3x3::Matrix3x3()
{
	for (int c = 0; c < 3; c++)
		for (int l = 0; l < 3; l++)
			values[l][c] = c == l ? 1 : 0;
}
Matrix3x3::Matrix3x3(float a, float b, float c, float d, float e, float f, float g, float h, float k)
{
	values M_A = a;
	values M_B = b;
	values M_C = c;

	values M_D = d;
	values M_E = e;
	values M_F = f;

	values M_G = g;
	values M_H = h;
	values M_K = k;
}
Matrix3x3::Matrix3x3(const Matrix3x3 & toCopy)
{
	for (int c = 0; c < 3; c++)
		for (int l = 0; l < 3; l++)
			values[l][c] = toCopy.values[l][c];
}
#pragma endregion

#pragma region Transformations
Matrix3x3 Matrix3x3::translate(float x, float y)
{
	*this *= translationMatrix(x, y).transposed();
	return *this;
}
Matrix3x3 Matrix3x3::rotate(float deg)
{
	*this *= rotationMatrix(deg).transposed();
	return *this;
}
Matrix3x3 Matrix3x3::scale(float x, float y)
{
	*this *= scaleMatrix(x, y).transposed();
	return *this;
}
#pragma endregion

#pragma region Utils
float Matrix3x3::det2x2(float a, float b, float c, float d)
{
	return (+(a*d)) + (-(b*c));
}
ofMatrix4x4 Matrix3x3::toOfMatrix4x4()
{
	ofMatrix4x4 m4x4 = ofMatrix4x4
	(
		a(), b(), 0, c(),
		d(), e(), 0, f(),
		0, 0, 1, 0,
		g(), h(), 0, k()
	);
	return m4x4;
}
ofVec2f Matrix3x3::transform(const ofVec2f& vector, float z) const
{
	float c11 = values M_C + (values M_B * vector.y) + (values M_A * vector.x);
	float c21 = values M_F + (values M_E * vector.y) + (values M_D * vector.x);

	return ofVec2f(c11, c21);
}
#pragma endregion

#pragma region Properties
float Matrix3x3::det()
{
	return (+(a()*e()*k())) + (+(b()*f()*g())) + (+(c()*d()*h())) + (-(c()*e()*g())) + (-(a()*f()*h())) + (-(b()*d()*k()));
}
Matrix3x3 Matrix3x3::cof()
{
	float a11 = pow(-1, 2) * det2x2(e(), f(), h(), k());
	float a12 = pow(-1, 3) * det2x2(d(), f(), g(), k());
	float a13 = pow(-1, 4) * det2x2(d(), e(), g(), h());

	float a21 = pow(-1, 3) * det2x2(b(), c(), h(), k());
	float a22 = pow(-1, 4) * det2x2(a(), c(), g(), k());
	float a23 = pow(-1, 5) * det2x2(a(), b(), g(), h());

	float a31 = pow(-1, 4) * det2x2(b(), c(), e(), f());
	float a32 = pow(-1, 5) * det2x2(a(), c(), d(), f());
	float a33 = pow(-1, 6) * det2x2(a(), b(), d(), e());

	return Matrix3x3(a11, a12, a13, a21, a22, a23, a31, a32, a33);
}
Matrix3x3 Matrix3x3::adj() { return	cof().transposed(); }
Matrix3x3 Matrix3x3::inversed()
{
	if (det() != 0)
		return adj() / det();
	else
		return Matrix3x3() * -1;
}
Matrix3x3 Matrix3x3::transposed()
{
	const float a11 = values M_A;
	const float a12 = values M_D;
	const float a13 = values M_G;

	const float a21 = values M_B;
	const float a22 = values M_E;
	const float a23 = values M_H;

	const float a31 = values M_C;
	const float a32 = values M_F;
	const float a33 = values M_K;

	return Matrix3x3(a11, a12, a13, a21, a22, a23, a31, a32, a33);
}
#pragma endregion

#pragma region Factories
Matrix3x3 Matrix3x3::translationMatrix(float x, float y)
{
	Matrix3x3 temp = Matrix3x3();
	temp.values M_C = x;
	temp.values M_F = y;
	return temp;
}
Matrix3x3 Matrix3x3::rotationMatrix(float deg)
{
	Matrix3x3 temp = Matrix3x3();
	float rad = deg * M_PI / 180;

	temp.values M_A = cos(rad);
	temp.values M_B = -sin(rad);
	temp.values M_D = sin(rad);
	temp.values M_E = cos(rad);

	return temp;
}
Matrix3x3 Matrix3x3::scaleMatrix(float x, float y)
{
	Matrix3x3 temp = Matrix3x3();

	temp.values M_A = x;
	temp.values M_E = y;

	return temp;
}
#pragma endregion

#pragma region Operators
Matrix3x3 Matrix3x3::operator = (const Matrix3x3 toCopy)
{
	for (int c = 0; c < 3; c++)
		for (int l = 0; l < 3; l++)
			values[l][c] = toCopy.values[l][c];
	return *this;
}
Matrix3x3 Matrix3x3::operator * (const float scalar) const
{
	Matrix3x3 copy = Matrix3x3(*this);
	for (int l = 0; l < 3; l++)
		for (int c = 0; c < 3; c++)
			copy.values[l][c] *= scalar;
	return copy;
}
Matrix3x3 Matrix3x3::operator * (const Matrix3x3 other) const
{
	const float c11 = (values M_A * other.values M_A) + (values M_B * other.values M_D) + (values M_C * other.values M_G);
	const float c12 = (values M_A * other.values M_B) + (values M_B * other.values M_E) + (values M_C * other.values M_H);
	const float c13 = (values M_A * other.values M_C) + (values M_B * other.values M_F) + (values M_C * other.values M_K);

	const float c21 = (values M_D * other.values M_A) + (values M_E * other.values M_D) + (values M_F * other.values M_G);
	const float c22 = (values M_D * other.values M_B) + (values M_E * other.values M_E) + (values M_F * other.values M_H);
	const float c23 = (values M_D * other.values M_C) + (values M_E * other.values M_F) + (values M_F * other.values M_K);

	const float c31 = (values M_G * other.values M_A) + (values M_H * other.values M_D) + (values M_K * other.values M_G);
	const float c32 = (values M_G * other.values M_B) + (values M_H * other.values M_E) + (values M_K * other.values M_H);
	const float c33 = (values M_G * other.values M_C) + (values M_H * other.values M_F) + (values M_K * other.values M_K);

	return Matrix3x3(c11, c12, c13, c21, c22, c23, c31, c32, c33);
}
Matrix3x3 Matrix3x3::operator / (const float scalar) const
{
	Matrix3x3 copy = Matrix3x3(*this);
	for (int l = 0; l < 3; l++)
		for (int c = 0; c < 3; c++)
			copy.values[l][c] /= scalar;
	return copy;
}

Matrix3x3 Matrix3x3::operator *= (const float scalar)
{
	Matrix3x3* self = this;
	*self = *self * scalar;
	return *self;
}
Matrix3x3 Matrix3x3::operator *= (const Matrix3x3 other)
{
	Matrix3x3* self = this;
	*self = *self * other;
	return *self;
}
Matrix3x3 Matrix3x3::operator /= (const float scalar)
{
	Matrix3x3* self = this;
	*self = *self / scalar;
	return *self;
}
float	  Matrix3x3::operator () (const int row, const int col) const
{
	if (row >= 1 && row <= 3 && col >= 1 && col <= 3) return values[row - 1][col - 1];
}
Matrix3x3 Matrix3x3::operator + (const Matrix3x3 other) const
{
	float c11 = values M_A + other.values M_A;
	float c12 = values M_B + other.values M_B;
	float c13 = values M_C + other.values M_C;

	float c21 = values M_D + other.values M_D;
	float c22 = values M_E + other.values M_E;
	float c23 = values M_F + other.values M_F;

	float c31 = values M_G + other.values M_G;
	float c32 = values M_H + other.values M_H;
	float c33 = values M_K + other.values M_K;

	return Matrix3x3(c11, c12, c13, c21, c22, c23, c31, c32, c33);
}
Matrix3x3 Matrix3x3::operator - (const Matrix3x3 other) const
{
	float c11 = values M_A - other.values M_A;
	float c12 = values M_B - other.values M_B;
	float c13 = values M_C - other.values M_C;

	float c21 = values M_D - other.values M_D;
	float c22 = values M_E - other.values M_E;
	float c23 = values M_F - other.values M_F;

	float c31 = values M_G - other.values M_G;
	float c32 = values M_H - other.values M_H;
	float c33 = values M_K - other.values M_K;

	return Matrix3x3(c11, c12, c13, c21, c22, c23, c31, c32, c33);
}
Matrix3x3 Matrix3x3::operator += (Matrix3x3 other)
{
	Matrix3x3 self = *this;
	self = self + other;
	return self;
}
Matrix3x3 Matrix3x3::operator -= (const Matrix3x3 other)
{
	Matrix3x3 self = *this;
	self = self - other;
	return self;
}
#pragma endregion

#pragma region Sintax Sugar
const float Matrix3x3::a() { return values M_A; }
const float Matrix3x3::b() { return values M_B; }
const float Matrix3x3::c() { return values M_C; }

const float Matrix3x3::d() { return values M_D; }
const float Matrix3x3::e() { return values M_E; }
const float Matrix3x3::f() { return values M_F; }

const float Matrix3x3::g() { return values M_G; }
const float Matrix3x3::h() { return values M_H; }
const float Matrix3x3::k() { return values M_K; }
#pragma endregion


