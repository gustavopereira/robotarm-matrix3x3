#pragma once
#include <math.h>
#include <ofMatrix4x4.h>

#define M_PI 3.14159265358979323846

#define M_A [0][0]
#define M_B [0][1]
#define M_C [0][2]

#define M_D [1][0]
#define M_E [1][1]
#define M_F [1][2]

#define M_G [2][0]
#define M_H [2][1]
#define M_K [2][2]

class Matrix3x3
{
private:
	//Utils
	float det2x2(float a, float b, float c, float d);
public:
	//Contructors
	Matrix3x3(); // -> Identity
	Matrix3x3(float a, float b, float c, float d, float e, float f, float g, float h, float k);
	Matrix3x3(const Matrix3x3 & toCopy);

	//Values
	float values[3][3];

	//Factories
	static Matrix3x3 translationMatrix(float x, float y);
	static Matrix3x3 rotationMatrix(float deg);
	static Matrix3x3 scaleMatrix(float x, float y);

	//Operations
	Matrix3x3 translate(float x, float y);
	Matrix3x3 rotate(float deg);
	Matrix3x3 scale(float x, float y);

	//Properties
	float det();
	Matrix3x3 cof();
	Matrix3x3 adj();
	Matrix3x3 inversed();
	Matrix3x3 transposed();

	//Utils
	ofMatrix4x4 Matrix3x3::toOfMatrix4x4();
	ofVec2f Matrix3x3::transform(const ofVec2f& vector, float z = 1.0f) const;

	//Sintax Sugar
	const float a();	const float b();	const float c();
	const float d();	const float e();	const float f();
	const float g();	const float h();	const float k();	

	//Operators
	Matrix3x3 operator =  (const Matrix3x3 toCopy);
	float     operator () (const int row, const int col) const;

	Matrix3x3 operator +  (const Matrix3x3 other) const;
	Matrix3x3 operator -  (const Matrix3x3 other) const;
	Matrix3x3 operator *  (const float scalar) const;
	Matrix3x3 operator *  (const Matrix3x3 other) const;
	Matrix3x3 operator /  (const float scalar) const;

	Matrix3x3 operator += (const Matrix3x3 other);
	Matrix3x3 operator -= (const Matrix3x3 other);
	Matrix3x3 operator *= (const float scalar);
	Matrix3x3 operator *= (const Matrix3x3 other);
	Matrix3x3 operator /= (const float scalar);
};

