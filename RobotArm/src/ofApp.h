#pragma once

#include "ofMain.h"
#include "Matrix3x3.h"

#define ROTATION_SPEED 15

class ofApp : public ofBaseApp {

public:
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	Matrix3x3 armMatrix;
	Matrix3x3 forearmMatrix;
	Matrix3x3 handMatrix;

	float armDegrees;
	float forearmDegrees;
	float handDegrees;

	ofImage arm;
	ofImage forearm;
	ofImage hand;
};
