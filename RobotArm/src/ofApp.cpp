#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	arm.load("arm.png");
	forearm.load("arm.png");
	hand.load("hand.png");
}

//--------------------------------------------------------------
void ofApp::update() {

}

//--------------------------------------------------------------
void ofApp::draw() {
	ofPushMatrix();

	//Arm
	armMatrix = Matrix3x3();
	armMatrix.translate(-arm.getWidth() / 10, -arm.getHeight() / 2);
	armMatrix.rotate(armDegrees);
	armMatrix.translate(400, 300);

	ofMultMatrix(armMatrix.toOfMatrix4x4());
	arm.draw(0, 0);

	//Forearm
	forearmMatrix = Matrix3x3();
	forearmMatrix.translate(-forearm.getWidth() / 9, -forearm.getHeight() / 2);
	forearmMatrix.rotate(forearmDegrees);
	forearmMatrix.translate(124, 13);

	ofMultMatrix(forearmMatrix.toOfMatrix4x4());
	forearm.draw(0, 0);

	//Hand
	handMatrix = Matrix3x3();
	handMatrix.translate(-hand.getWidth() / 6, -hand.getHeight() / 2);
	handMatrix.rotate(handDegrees);
	handMatrix.translate(124, 13);

	ofMultMatrix(handMatrix.toOfMatrix4x4());
	hand.draw(0, 0);

	ofPopMatrix();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	if (key == 'Q' || key == 'q')			handDegrees -= ROTATION_SPEED;
	else if (key == 'E' || key == 'e')		handDegrees += ROTATION_SPEED;

	if (key == 'A' || key == 'a')			forearmDegrees -= ROTATION_SPEED;
	else if (key == 'D' || key == 'd')		forearmDegrees += ROTATION_SPEED;

	if (key == 'Z' || key == 'z')			armDegrees -= ROTATION_SPEED;
	else if (key == 'C' || key == 'c')		armDegrees += ROTATION_SPEED;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
